# Bradipo #

| Branch | Pipeline Status |Code Coverage |
| ------------- |:--------------|---|
| master      | [![pipeline status](https://gitlab.com/darwinsw/bradipo/badges/master/pipeline.svg)](https://gitlab.com/darwinsw/bradipo/commits/master) | [![coverage](https://gitlab.com/darwinsw/bradipo/badges/master/coverage.svg)](https://darwinsw.gitlab.io/bradipo/coverage/master/html-report/index.htm)|
| release | [![pipeline status](https://gitlab.com/darwinsw/bradipo/badges/release/pipeline.svg)](https://gitlab.com/darwinsw/bradipo/commits/release) | [![coverage](https://gitlab.com/darwinsw/bradipo/badges/release/coverage.svg)](https://darwinsw.gitlab.io/bradipo/coverage/release/html-report/index.htm)|
| ci-dev | [![pipeline status](https://gitlab.com/darwinsw/bradipo/badges/ci-dev/pipeline.svg)](https://gitlab.com/darwinsw/bradipo/commits/ci-dev) | [![coverage](https://gitlab.com/darwinsw/bradipo/badges/ci-dev/coverage.svg)](https://darwinsw.gitlab.io/bradipo/coverage/ci-dev/html-report/index.htm)|

# Summary

A [*.Net Standard*](https://docs.microsoft.com/en-us/dotnet/standard/net-standard) compliant .Net library, providing some basic extension methods to reuse across projects.
