using System;

namespace Bradipo.Fp {
    public static class LetExtension {
        public static void Let<T>(this T self, Action<T> action, Action onNull = null) {
            if (self != null) {
                action(self);
            } else {
                onNull?.Invoke();
            }
        }
        
        public static TResult Let<T, TResult>(this T self, Func<T, TResult> func, Func<TResult> defaultValueProducer) {
            if (self != null) {
                return func(self);
            }
            return defaultValueProducer();
        }

        public static TResult Let<T, TResult>(this T self, Func<T, TResult> func, TResult defaultValue) {
            return self.Let(func, () => defaultValue);
        }
        
        public static TResult Let<T, TResult>(this T self, Func<T, TResult> func) {
            return self.Let(func, () => default(TResult));
        }
    }
}