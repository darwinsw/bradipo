using System;
using System.Collections.Generic;

namespace Bradipo {
    public static class DictionaryExtension {
        public static TValue GetOrElse<TKey, TValue>(this IDictionary<TKey, TValue> self, TKey key, TValue alternativeValue) {
            return self.ContainsKey(key) ? self[key] : alternativeValue;
        }
        
        public static TValue GetOrElse<TKey, TValue>(this IDictionary<TKey, TValue> self, TKey key, Func<TValue> alternativeValueProducer) {
            return self.ContainsKey(key) ? self[key] : alternativeValueProducer();
        }
        
        public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> self, TKey key) {
            return self.GetOrElse(key, default(TValue));
        }
    }
}