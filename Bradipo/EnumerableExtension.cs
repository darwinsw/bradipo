using System.Collections.Generic;

namespace Bradipo {
    public static class EnumerableExtension {
        public static IEnumerable<TItem> AsEnumerable<TItem>(this TItem self) {
            return new[] { self };
        }
        
        public static IList<TItem> AsList<TItem>(this TItem self) {
            return new[] { self };
        }
    }
}