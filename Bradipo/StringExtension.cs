﻿namespace Bradipo {
    public static class StringExtension {
        public static string OrEmpty(this string self) {
            return self.Or(string.Empty);
        }
    }
}