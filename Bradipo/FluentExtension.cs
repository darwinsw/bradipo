using System;

namespace Bradipo {
    public static class FluentExtension {
        public static T Or<T>(this T self, T defaultValue) {
            if (self != null) {
                return self;
            }
            return defaultValue;
        }
        
        public static T Or<T>(this T self, Func<T> defaultValueProvider) {
            if (self != null) {
                return self;
            }
            return defaultValueProvider();
        }
    }
}