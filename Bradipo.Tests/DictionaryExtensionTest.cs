using System.Collections.Generic;
using System.Linq;
using FactAttribute = Xunit.FactAttribute;
using NHamcrest;
using NHamcrest.XUnit;

namespace Bradipo.Tests {
    public class DictionaryExtensionTest {
        [Fact]
        public void ShouldReturnPresentValueByKey() {
            Assert.That(new Dictionary<string, string> {{"x", "xValue"}}.GetOrElse("x", "anotherValue"), Is.EqualTo("xValue"));
        }

        [Fact]
        public void ShouldReturnAlternativeValueWhenKeyIsNotPresent() {
            Assert.That(new Dictionary<string, string> {{"y", "yValue"}}.GetOrElse("x", "anotherValue"), Is.EqualTo("anotherValue"));
        }

        [Fact]
        public void ShouldReturnDefaultStringValueWhenKeyIsNotPresent() {
            Assert.That(new Dictionary<string, string> {{"x", "xValue"}}.GetOrDefault("y"), Is.Null());
        }
        
        [Fact]
        public void ShouldReturnDefaultIn32ValueWhenKeyIsNotPresent() {
            Assert.That(new Dictionary<string, int> {{"x", 19}}.GetOrDefault("y"), Is.EqualTo(default(int)));
        }
        
        [Fact]
        public void ShouldUseAlternativeValueProducerWhenKeyIsNotPresent() {
            Assert.That(new Dictionary<string, string> {{"y", "yValue"}}.GetOrElse("x", () => "anotherValue"), Is.EqualTo("anotherValue"));
        }
        
        [Fact]
        public void ShouldNotUseAlternativeValueProducerWhenKeyIsPresent() {
            bool called = false;
            new Dictionary<string, string> {{"x", "xValue"}}.GetOrElse("x", () => {
                called = true;
                return "anotherValue";
            });
            Assert.That(called, Is.False());
        }
    }
}