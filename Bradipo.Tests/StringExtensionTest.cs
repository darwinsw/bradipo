using FactAttribute = Xunit.FactAttribute;
using NHamcrest;
using NHamcrest.XUnit;

namespace Bradipo.Tests {
    public class StringExtensionTest {
        [Fact]
        public void ShouldUseDefaultValueWhenSelfIsNull() {
            string value = null;
            Assert.That(value.OrEmpty(), Is.EqualTo(string.Empty));
        }
        
        [Fact]
        public void ShouldSelfWhenItIsNotNull() {
            string value = "Hello";
            Assert.That(value.OrEmpty(), Is.EqualTo(value));
        }
    }
}