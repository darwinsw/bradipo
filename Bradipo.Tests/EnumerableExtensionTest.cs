using System.Collections.Generic;
using System.Linq;
using FactAttribute = Xunit.FactAttribute;
using NHamcrest;
using NHamcrest.XUnit;

namespace Bradipo.Tests {
    public class EnumerableExtensionTest {
        [Fact]
        public void CanCreateEnumerableFromSingleElementFluently_object() {
            object value = new object();
            IEnumerable<object> objects = value.AsEnumerable();
            Assert.That(objects.Single(), Is.EqualTo(value));
        }
        
        [Fact]
        public void CanCreateEnumerableFromSingleElementFluently_string() {
            const string value = "AAA";
            IEnumerable<string> strings = value.AsEnumerable();
            Assert.That(strings.Single(), Is.EqualTo(value));
        }
        
        [Fact]
        public void CanCreateListFromSingleElementFluently() {
            const int value = 19;
            IList<int> ints = value.AsList();
            Assert.That(ints.Single(), Is.EqualTo(value));
        }
    }
}