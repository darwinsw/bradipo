using System;
using FactAttribute = Xunit.FactAttribute;
using NHamcrest;
using NHamcrest.XUnit;

namespace Bradipo.Tests {
    public class FluentExtensionTest {
        [Fact]
        public void ShouldUseSelfWhenItIsNotNull() {
            string value = "abc";
            var result = value.Or("def");
            Assert.That(result, Is.EqualTo(value));
        }
        
        [Fact]
        public void ShouldUseDefaultValueWhenSelfIsNull_Class() {
            string value = null;
            string defaultValue = "def";
            var result = value.Or(defaultValue);
            Assert.That(result, Is.EqualTo(defaultValue));
        }
        
        [Fact]
        public void ShouldUseDefaultValueWhenSelfIsNull_Struct() {
            DateTimeOffset? value = null;
            var now = DateTimeOffset.Now;
            var result = value.Or(now);
            Assert.That(result, Is.EqualTo((DateTimeOffset?) now));
        }
        
        [Fact]
        public void ShouldUseDefaultValueProviderWhenSelfIsNull() {
            string value = null;
            string defaultValue = "def";
            var result = value.Or(() => defaultValue);
            Assert.That(result, Is.EqualTo(defaultValue));
        }
        
        [Fact]
        public void ShouldNotUseDefaultValueProviderWhenSelfIsNotNull() {
            string value = "abc";
            string defaultValue = "def";
            bool called = false;
            var result = value.Or(() => {
                called = true;
                return defaultValue;
            });
            Assert.That(called, Is.False());
        }
    }
}