using System;
using Bradipo.Fp;
using NHamcrest;
using Xunit;
using Assert = NHamcrest.XUnit.Assert;

namespace Bradipo.Tests.Fp {
    public class LetExtensionTest {
        [Fact]
        public void ShouldCallActionPassingItTheValueWhenTheValueIsNotNull() {
            string text = "A message";
            string result = string.Empty;
            text.Let(val => {
                result = val;
            });
            Assert.That(result, Is.EqualTo(text));
        }
        
        [Fact]
        public void ShouldNotCallActionPassingItTheValueWhenTheValueIsNull() {
            string text = null;
            string result = string.Empty;
            text.Let(val => {
                result = val;
            });
            Assert.That(result, Is.EqualTo(string.Empty));
        }
        
        [Fact]
        public void ShouldCallErrorCallbackWhenTheValueIsNull() {
            string text = null;
            string result = string.Empty;
            string error = string.Empty;
            text.Let(val => result = val, () => error = "Error");
            Assert.That(error, Is.EqualTo("Error"));
        }

        [Fact]
        public void ShouldCallFunctionPassingItTheValueWhenTheValueIsNotNull() {
            string text = "A message";
            string result = text.Let(x => x.ToUpper());
            Assert.That(result, Is.EqualTo(text.ToUpper()));            
        }
        
        [Fact]
        public void ShouldReturnNullWhenTheStringValueIsNull() {
            string text = null;
            string result = text.Let(x => x.ToUpper());
            Assert.That(result, Is.Null());            
        }
        
        [Fact]
        public void ShouldReturnNullWhenTheNullableDateTimeOffsetValueIsNull() {
            DateTimeOffset? input = null;
            string result = input.Let(x => x.ToString());
            Assert.That(result, Is.Null());            
        }
        
        [Fact]
        public void ShouldReturnDefaultValueWhenTheValueIsNullAndADefaultIsProvided_Class() {
            string input = null;
            string result = input.Let(x => x.ToString(), "Default value");
            Assert.That(result, Is.EqualTo("Default value"));            
        }
        
        [Fact]
        public void ShouldReturnDefaultValueWhenTheValueIsNullAndADefaultIsProvided_Struct() {
            string input = null;
            var now = DateTimeOffset.Now;
            DateTimeOffset result = input.Let(DateTimeOffset.Parse, now);
            Assert.That(result, Is.EqualTo(now));            
        }
        
        [Fact]
        public void CanSpecifyDefaultValueThroughFunction() {
            string input = null;
            string result = input.Let(x => x.ToString(), () => "Default value");
            Assert.That(result, Is.EqualTo("Default value"));            
        }
    }
}